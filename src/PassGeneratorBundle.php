<?php

namespace PassGeneratorBundle;

use PassGeneratorBundle\DependencyInjection\PassGeneratorExtension;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;

class PassGeneratorBundle extends AbstractBundle
{
    public function getContainerExtension(): ?ExtensionInterface
    {
        return new PassGeneratorExtension();
    }
}