<?php

namespace PassGeneratorBundle\Service;

class PassGenerator
{
    public function __construct(
        private readonly bool $numbers,
        private readonly bool $upperCase,
        private readonly bool $lowerCase,
        private readonly bool $specialChars,
        private readonly int $length,
        private readonly PassContentsInterface $passContents
    ) {
    }

    /**
     * @return string
     */
    public function generatePassword(): string
    {
        $password = '';

        $symbolsAvailable = [];
        if ($this->numbers) $symbolsAvailable = array_merge($symbolsAvailable, $this->passContents->getNumbers());
        if ($this->upperCase) $symbolsAvailable = array_merge($symbolsAvailable, $this->passContents->getUpperCases());
        if ($this->lowerCase) $symbolsAvailable = array_merge($symbolsAvailable, $this->passContents->getLowerCases());
        if ($this->specialChars) $symbolsAvailable = array_merge($symbolsAvailable, $this->passContents->getSpecialChars());

        for ($i = 0; $i < $this->length; $i++) {
            $password .= $symbolsAvailable[mt_rand(0, count($symbolsAvailable) - 1)];
        }

        return $password;
    }
}