<?php

namespace PassGeneratorBundle\Service;

interface PassContentsInterface
{
    /**
     * @return string[]
     */
    public function getNumbers(): array;

    /**
     * @return string[]
     */
    public function getUpperCases(): array;

    /**
     * @return string[]
     */
    public function getLowerCases(): array;

    /**
     * @return string[]
     */
    public function getSpecialChars(): array;
}