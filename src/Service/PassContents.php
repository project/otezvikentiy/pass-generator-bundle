<?php

namespace PassGeneratorBundle\Service;

class PassContents implements PassContentsInterface
{
    /**
     * @return string[]
     */
    public function getNumbers(): array
    {
        return ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'];
    }

    /**
     * @return string[]
     */
    public function getUpperCases(): array
    {
        return ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
    }

    /**
     * @return string[]
     */
    public function getLowerCases(): array
    {
        return ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
    }

    /**
     * @return string[]
     */
    public function getSpecialChars(): array
    {
        return ['!', '#', '$', '%', '&', '(', ')', '*', '+', '.', '/', ':', ';', '=', '>', '?', '@', '[', ']', '^', '`', '{', '|', '}', '~'];
    }
}